const FILE_LOG = "services/users.ts";

/**
 * Retorna todos los usuarios
 *
 * @return      listado de usuarios o undefined
 *
 */
export function getUser(username: string): any {

  try {

    const users = [
      {
        username: "nicolas",
        password: "418d940643b1975d62234ee01246ad4b58904184"
      }
    ]

    return users.find( (user) => user.username === username);

  } catch (error) {
    console.error(`${FILE_LOG} (getUser): Se produjo un error al realizar la operación (${error.stack})`);
  }

}
