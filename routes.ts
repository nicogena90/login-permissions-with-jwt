import { Router } from "express";
import * as controller from "./controllers/users";
import * as mdAuth from "./middlewares/authentication";

const api: Router = Router();

// El ednpoint de login no tiene middleware para verificar token
api.post("/login",                       controller.login);

// Los demas endpoint se verifica que si tienen el token valido
api.get("/users",   mdAuth.ensureAuth,   controller.getUsers);
api.post("/user",   mdAuth.ensureAuth,   controller.addUser);
api.put("/user",    mdAuth.ensureAuth,   controller.editUser);

export default api;