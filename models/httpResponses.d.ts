declare namespace app.httpResponses {

  export const enum errorCode {
    OK = 200,
    BAD_REQUEST = 400,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500
  }

  export const enum errorMsgs {
    MISSING_FIELDS = "Error debe completar todos los campos",
    WRONG_TYPE = "Error debe ingresar correctamente los campos"
  }

  export interface response {
    data?: any;
    errorMsg?: string;
  }
  
}