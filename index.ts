import * as express from "express";
import * as bodyParser from "body-parser";
const app = express();

import { headers } from "./middlewares/headers";

const port = 5000;

import appRoutes from "./routes";

// Body parser
app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(express.json({limit: "50mb"}));
app.use(headers);

// Rutas base
const routes = [
  appRoutes
]

// Se cargan las rutas
app.use('/', routes);

// Escucha en el puerto 5000
app.listen(port, () => {
  console.log(`Servidor inicializado en el puerto 5000.  Accede desde la URL http://localhost:5000`);
});
