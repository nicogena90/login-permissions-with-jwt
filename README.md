# Login and access with JWT

Ejemplo practico de como manejar el login y acceso de una aplicacion con node en typescript y JWT (JSON WEB TOKEN).

## Que es un token de acceso?

El token de acceso es un código que se utiliza en los sistemas para verificar la autenticidad y permisos de un usuario realizando una petición a un servidor.

Primero se realiza el login del usuario enviando un username y contraseña al servidor, este devuelve, teniendo en cuenta la valides de estos datos, un token.

El usuario utilizara este token para validar las siguientes peticiones que haga al servidor y así verificar que es un usuario habilitado a realizarlas.

El servidor verificara la valides de este token con una pass configurada y un tiempo de expiración, también configurable, del token.

## Como correr el proyecto?

Una vez clonado el repositorio localmente se deben instalar las dependencias con el siguiente comando

```
npm i
```

Una vez instaladas las dependencias para levantar la aplicación se debe correr

```
npm run start
```

Listo! la aplicación estará escuchando en el puerto 5000 las diferentes peticiones.

## Ejemplos de peticiones

En el proyecto actual existen los siguientes endpoints de ejemplo para crear los necesarios de tu aplicación.

### El de login es el unico endpoint que no verifica que se envie el token

POST    /login
body: {
    "username": "nicolas",
    "password": "nicolas"
}"

"response": {
    "data": {
        "username": "nicolas",
        "password": null,
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI0Y2JjMGRiMmI3ZDBmM2VkNDM5ZDJhZGM4ZGEyNGQ4ZGJiZTZhMTQ1IiwidXNlcm5hbWUiOiJuaWNvbGFzIiwicGFzc3dvcmQiOiI0MThkOTQwNjQzYjE5NzVkNjIyMzRlZTAxMjQ2YWQ0YjU4OTA0MTg0IiwiaWF0IjoxNTk1MDQyMDE1LCJleHAiOjE1OTU2NDY4MTV9.clI0scb6c4L8TKAfQvIZ9XG_7Y8vWwhJ4LLTIeooJik"
    }
}

### Estos son endpoints de ejemplo donde se verifica el token, solo devuelven un texto
GET     /users
POST    /user
PUT     /user
