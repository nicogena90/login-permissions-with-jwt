import { NextFunction, Request, Response } from "express";
import * as jwt from "../utils/jwt";
import * as httpResp from "../controllers/httpResponses";

/**
 * Función de middleware para la autenticación del usuario
 *
 * @param req   Request
 * @param res   Response
 * @param next  NextFunction
 */
export async function ensureAuth(req: Request, res: Response, next: NextFunction): Promise<void> {


  try {

    // Verifico que el request tenga cabecera de autenticación
    if (req.headers.authorization) {

      // Se quitan las comillas de apertura y cierre
      const token = req.headers.authorization.replace(/['"]+/g, "");

      // Verifivo la fecha de expiración del token, si es superior a la actual el token es valido
      if (jwt.checkToken(token)) {

        // Permito seguir utilizandolo
        next();
        return;

      } else {
        httpResp.error(
          res,
          app.httpResponses.errorCode.FORBIDDEN,
          `El token no es valido`
        );
      }

    } else {
      httpResp.error(
        res,
        app.httpResponses.errorCode.BAD_REQUEST,
        `La petición no esta autenticada`,
      );
    }

  } catch (error) {
    httpResp.error(
      res,
      app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `Autenticación fallida`);
  }

}

