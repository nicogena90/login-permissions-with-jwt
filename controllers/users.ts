import { Request, Response } from "express";

import * as crypto from "../utils/crypto";
import * as jwt from "../utils/jwt";
import * as httpResp from "./httpResponses";
import * as srvUsers from "../services/users";

const FILE_LOG = "controllers/users.ts";

/**
 * Función para verificar contraseña y devolver el token generado
 *
 * @param req   Request
 * @param res   Response
 */
export function login(req: Request, res: Response) {
  
  try {
    // Credenciales
    const {
      username = null,
      password = null
    } = req.body;

    // Valido los datos enviados en el Request
    if (username !== null && password !== null) {

      // Funcion para verificar las credenciales
      const user = srvUsers.getUser(username);

      // Se verifica el usuario
      if (user) {

        // Encripto y comparo la password ingresada con la almacenada
        const passwordParam = crypto.encode(password);

        if (user.password === passwordParam) {

          // Genero un nuevo token y lo retorno
          const hash = jwt.createToken(user);

          // Blanqueo la password del  Usuario
          user.password = null;

          // Retorno el token con el usuario
          httpResp.success(res, { ...user, token: hash });

        } else {
          httpResp.error(res, app.httpResponses.errorCode.FORBIDDEN,
            `Password incorrecto`);
        }

      } else {
        httpResp.error(res, app.httpResponses.errorCode.NOT_FOUND,
          `El usuario ${username} no existe`);
      }

    } else {
      httpResp.error(res, app.httpResponses.errorCode.BAD_REQUEST,
        `Todos los campos deben ser completados`);
    }

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (login): error (${error.stack})`);
  }
}

/**
 * Función para traer los usuarios
 *
 * @param req   Request
 * @param res   Response
 */
export function getUsers(req: Request, res: Response) {
  
  try {

    // Retorno lista de usuarios
    httpResp.success(res, "Lista de usuario");

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (getUsers): error (${error.stack})`);
  }
}

/**
 * Función para insertar un usuario
 *
 * @param req   Request
 * @param res   Response
 */
export function addUser(req: Request, res: Response) {
  
  try {

    // Se agrega un usuario
    httpResp.success(res, "Usuario insertado");

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (addUser): error (${error.stack})`);
  }
}

/**
 * Función para editar un usuario
 *
 * @param req   Request
 * @param res   Response
 */
export function editUser(req: Request, res: Response) {
  
  try {

    // Se edita un usuario
    httpResp.success(res, "Usuario actualizado");

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (editUser): error (${error.stack})`);
  }
}