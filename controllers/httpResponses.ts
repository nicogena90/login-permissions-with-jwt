import { Response } from "express";

/**
 * Genera y envia una respuesta exitosa
 *
 * @param res           response
 * @param data          datos de retorno
 *
 */
export function success(res: Response, data: any): void {

  const response: app.httpResponses.response = {
    data
  };

  res.status(app.httpResponses.errorCode.OK).send({ response });
}

/**
 * Genera y envia una respuesta con error
 *
 * @param res           response
 * @param errorCode     codigo del error
 * @param errorMsg      mensaje del error
 * 
 */
export function error(res: Response, errorCode: app.httpResponses.errorCode, errorMsg: string): void {

  const response: app.httpResponses.response = {
    errorMsg
  };

  console.error(errorMsg);
  res.status(errorCode).send({ response });
}
