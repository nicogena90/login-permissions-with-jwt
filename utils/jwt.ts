import * as jwt from "jwt-simple";
import * as moment from "moment";

// Constantes
const AUTH_KEY_USER = process.env.auth_key_user || "4cbc0db2b7d0f3ed439d2adc8da24d8dbbe6a145";
const AUTH_DAYS_EXP = process.env.auth_days_exp || "7";
const FILE_LOG = "utils/jwt.ts";

/**
 * Crea el token tomando como parametro el usuario
 *
 * @param user
 *
 */
export function createToken(user: any): string {

  const payload = {
    sub: AUTH_KEY_USER,
    username: user.username,
    password: user.password,
    iat: moment().unix(),
    exp: moment().add(AUTH_DAYS_EXP, "days").unix()
  };

  return jwt.encode(payload, AUTH_KEY_USER);
}

/**
 *  Decodifica el token
 *
 * @param token
 */
export function decodeToken(token: any): any {

  return jwt.decode(token, AUTH_KEY_USER, true);  
}


/**
 *  Verifica la expiración del token
 *
 * @param token
 */
export function checkToken(token: any): boolean {

  try {
    const payload = decodeToken(token);

    // Valido si expiró
    if (moment().unix() <= payload.exp) {
      return true;
    } else {
      return false;
    }

  } catch (error) {
    console.log(`${FILE_LOG} (checkToken): error (${error.stack})`);
    return false;
  }
}



