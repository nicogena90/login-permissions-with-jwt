import * as crypto from "crypto";

const FILE_LOG = "utils/crypto.ts";

/**
 * Genera un md5 sobre un contenido
 *
 * @param content        contenido a realizar el hash
 *
 * @return               md5 del contenido recibido
 *
 */
export function encode(content: string): string {

  try {

    // Obtiene una instancia de md5
    const hash = crypto.createHash("sha1");

    // Agrega el contenido del archivo
    hash.update(content);

    // Retorna el hash generado
    return hash.digest("hex");

  } catch (err) {
    console.error(`${FILE_LOG} (encode): (${err.stack})`);
  }

}
